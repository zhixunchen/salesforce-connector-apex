Salesforce Jira Connector Apex
==============================

This repository contains all [Apex](https://www.salesforce.com/us/developer/docs/apexcode/Content/apex_intro_what_is_apex.htm) codes for [Connector for Salesforce.com and JIRA](https://marketplace.atlassian.com/plugins/net.customware.plugins.connector.salesforce.salesforce-connector-plugin), including [Apex classes](https://www.salesforce.com/us/developer/docs/apexcode/Content/apex_classes_defining.htm), [Unit tests](http://help.salesforce.com/HTViewHelpDoc?id=code_run_tests.htm&language=en_US), [Visualforce pages](https://developer.salesforce.com/page/An_Introduction_to_Visualforce), and [Triggers](https://www.salesforce.com/us/developer/docs/apexcode/Content/apex_triggers.htm).

For more details, please refer to [ServiceRocket docs](https://docs.servicerocket.com/display/CFSJ/Writing+Apex+Class+to+Call+for+Connector+REST+APIs)
