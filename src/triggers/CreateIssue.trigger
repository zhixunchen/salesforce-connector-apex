trigger CreateIssue on Case (after insert) {

    // Check whether current user is not JIRA agent so that we don't create an infinite loop.
    if (JIRA.currentUserIsNotJiraAgent()) {
        for (Case c : Trigger.new) {
            // Define parameters to be used in calling Apex Class
            String objectType ='CASE'; // Please change this according to the object type
            String objectId = c.id;
            String projectKey = 'SFDC';
            String issueType = '1';
            // Calls the actual callout to create the JIRA issue.
            JIRAConnectorWebserviceCalloutCreate.createIssue(JIRA.baseUrl, JIRA.systemId, objectType, objectId, projectKey, issueType);
        }
    }

}